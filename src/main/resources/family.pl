parent('Alexandr','Olya').
parent('Masha','Olya').
parent('Alexandr','Alina').
parent('Masha','Alina').
parent('Alexandr','Leha').
parent('Masha','Leha').
parent('Leha','Gleb').
parent('Dasha','Gleb').
parent('Alina','Maks').
parent('Leha','Maks').
parent('Serhii_Konobas','Ihor_Konobas').
parent('Vitaliy_Konobas','Serhii_Konobas').
parent('Oleksandr_Kornienko','Anatoliy_Kornienko').
parent('Anatoliy_Kornienko','Oleh_Kornienko').
parent('Anatoliy_Kornienko','Miroslava_Kornienko').
parent('Olena_Konobas','Ihor_Konobas').
parent('Oleksandr_Kornienko','Olena_Konobas').
parent('Oleksandra_Kornienko','Olena_Konobas').
parent('Ludmila_Konobas','Serhii_Konobas').
parent('Leonid_Sherstyniuk','Nazar_Sherstyniuk').
parent('Leonid_Sherstyniuk','Bohdana_Sherstyniuk').
parent('Antonina_Sherstyniuk','Nazar_Sherstyniuk').
parent('Antonina_Sherstyniuk','Bohdana_Sherstyniuk').
parent('Ihor_Stoyanov','Anastasia_Stoyanova').
parent('Valentina_Stoyanova','Anastasia_Stoyanova').
parent('Valerii_Horohovskii','Dmytro_Horohovskii').
parent('Valerii_Horohovskii','Oksana_Horohovska').
parent('Hanna_Horohovska','Dmytro_Horohovskii').
parent('Hanna_Horohovska','Oksana_Horohovska').
parent('Mykola_Valkov','Antonina_Sherstyniuk').
parent('Mykola_Valkov','Valentina_Stoyanova').
parent('Antonina_Valkova','Antonina_Sherstyniuk').
parent('Antonina_Valkova','Valentina_Stoyanova').
parent('Leonid_Sherstyniuk1','Leonid_Sherstyniuk').
parent('Leonid_Sherstyniuk1','Hanna_Horohovska').
parent('Zoya_Sherstyniuk','Leonid_Sherstyniuk').
parent('Zoya_Sherstyniuk','Hanna_Horohovska').

man('Alexandr').
man('Leha').
man('Pavel').
man('Dima').
man('Ihor_Konobas').
man('Serhii_Konobas').
man('Vitaliy_Konobas').
man('Anatoliy_Kornienko').
man('Oleh_Kornienko').
man('Oleksandr_Kornienko').
man('Nazar_Sherstyniuk').
man('Leonid_Sherstyniuk').
man('Ihor_Stoyanov').
man('Valerii_Horohovskii').
man('Dmytro_Horohovskii').
man('Mykola_Valkov').
man('Leonid_Sherstyniuk1').

woman('Olya').
woman('Alina').
woman('Masha').
woman('Olena_Konobas').
woman('Oleksandra_Kornienko').
woman('Miroslava_Kornienko').
woman('Ludmila_Konobas').
woman('Bohdana_Sherstyniuk').
woman('Antonina_Sherstyniuk').
woman('Valentina_Stoyanova').
woman('Anastasia_Stoyanova').
woman('Oksana_Horohovska').
woman('Hanna_Horohovska').
woman('Antonina_Valkova').
woman('Zoya_Sherstyniuk').

married('Masha','Pavel').
married('Alina','Dima').
married('Olena_Konobas','Serhii_Konobas').
married('Oleksandr_Kornienko','Oleksandra_Kornienko').
married('Vitaliy_Konobas','Ludmila_Konobas').
married('Antonina_Sherstyniuk','Leonid_Sherstyniuk').
married('Valentina_Stoyanova','Ihor_Stoyanov').
married('Hanna_Horohovska','Valerii_Horohovskii').
married('Antonina_Valkova','Mykola_Valkov').
married('Zoya_Sherstyniuk','Leonid_Sherstyniuk1').

check_married(X, Y):-
  married(X, Y); married(Y, X).

father(X,Y):-
  parent(X,Y),man(X).

mother(X,Y):-
  parent(X,Y),woman(X).

sister(X,Y):-
  parent(Z,X), parent(Z,Y), woman(X), X\=Y.

brother(X,Y):-
  parent(Z,Y),parent(Z,X),man(X), X\=Y.

son(X,Y):-
  parent(Y,X),man(X).

daughter(X,Y):-
  parent(Y,X),woman(X).

wife(X,Y):-
  check_married(X,Y), woman(X).

husband(X,Y):-
  check_married(X,Y), man(X).

brother_in_law(X, Y):-
  wife(Y, Wife),
  brother(X, Wife).

mother_in_law(X,Y):-
  wife(Y, Wife),
  mother(X, Wife).

uncle(X, Y):-
  parent(Parent, Y),
  brother(X, Parent).
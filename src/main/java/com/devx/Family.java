package com.devx;

import org.projog.api.Projog;
import org.projog.api.QueryResult;
import org.projog.api.QueryStatement;

import java.io.File;
import java.util.*;

public class Family {

    private static final String FATHER_PREPARE_STATEMENT = "father(X,'%s').";
    private static final String MOTHER_PREPARE_STATEMENT = "mother(X,'%s').";
    private static final String SISTER_PREPARE_STATEMENT = "sister(X,'%s').";
    private static final String BROTHER_PREPARE_STATEMENT = "brother(X,'%s').";
    private static final String SON_PREPARE_STATEMENT = "son(X,'%s').";
    private static final String DAUGHTER_PREPARE_STATEMENT = "daughter(X,'%s').";
    private static final String WIFE_PREPARE_STATEMENT = "wife(X,'%s').";
    private static final String HUSBAND_PREPARE_STATEMENT = "husband(X,'%s').";
    private static final String BROTHER_IN_LAW_PREPARE_STATEMENT = "brother_in_law(X,'%s').";
    private static final String MOTHER_IN_LAW_PREPARE_STATEMENT = "mother_in_law(X,'%s').";
    private static final String UNCLE_PREPARE_STATEMENT = "uncle(X,'%s').";

    public static void main(String[] args) {
        Projog p = new Projog();
        p.consultFile(new File("src/main/resources/family.pl"));
        Scanner sc= new Scanner(System.in);
        while (true){
            System.out.println("Enter name:");
            String name = sc.next();
            System.out.print("father: ");
            System.out.println(getResult(String.format(FATHER_PREPARE_STATEMENT,name), p));
            System.out.print("mother: ");
            System.out.println(getResult(String.format(MOTHER_PREPARE_STATEMENT,name), p));
            System.out.print("sister: ");
            System.out.println(getResult(String.format(SISTER_PREPARE_STATEMENT,name), p));
            System.out.print("brother: ");
            System.out.println(getResult(String.format(BROTHER_PREPARE_STATEMENT,name), p));
            System.out.print("daughter: ");
            System.out.println(getResult(String.format(DAUGHTER_PREPARE_STATEMENT,name), p));
            System.out.print("son: ");
            System.out.println(getResult(String.format(SON_PREPARE_STATEMENT,name), p));
            System.out.print("wife: ");
            System.out.println(getResult(String.format(WIFE_PREPARE_STATEMENT,name), p));
            System.out.print("husband: ");
            System.out.println(getResult(String.format(HUSBAND_PREPARE_STATEMENT,name), p));
            System.out.print("brother in law: ");
            System.out.println(getResult(String.format(BROTHER_IN_LAW_PREPARE_STATEMENT,name), p));
            System.out.print("mother in law: ");
            System.out.println(getResult(String.format(MOTHER_IN_LAW_PREPARE_STATEMENT,name), p));
            System.out.print("uncle: ");
            System.out.println(getResult(String.format(UNCLE_PREPARE_STATEMENT,name), p));
        }
    }

    private static Set<String> getResult(String query, Projog projog){
        Set<String> results = new HashSet <>();
        QueryStatement statement = projog.query(query);
        QueryResult result = statement.getResult();
        while (result.next()) {
            results.add(result.getTerm("X").toString());
        }
        return results;
    }
}
